//
//  LanchViewController.swift
//  webView
//
//  Created by ahmed elmemy on 8/13/19.
//  Copyright © 2019 ElMeMy. All rights reserved.
//

import UIKit
import SwiftGifOrigin

class LanchViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.loadGif(asset: "splach")
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 5.5){
            
            let vc = Storyboard.Home.instantiate(ViewController.self)
            self.view.window?.rootViewController = vc
            
          
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
